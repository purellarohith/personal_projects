import React from 'react';
import ContactProvider from './src/context/contacts/contacts';
import SocketIO from './src/context/socket/socket';
import Route from './src/routes';

const App: React.FC = () => {
  return (
    <>
      <SocketIO>
        <ContactProvider>
          <Route />
        </ContactProvider>
      </SocketIO>
    </>
  );
};

export default App;
