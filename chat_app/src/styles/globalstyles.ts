import { StyleSheet } from 'react-native';

export const globalStyles = StyleSheet.create({
  main: {
    display: 'flex',
    flex: 1,
    backgroundColor: '#fff',
  },
  icon_wrapper: {
    minHeight: 24,
    minWidth: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
