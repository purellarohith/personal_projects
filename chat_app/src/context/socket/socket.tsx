import React, { createContext, useEffect, useRef } from 'react';
import { io, Socket, SocketOptions } from 'socket.io-client';

interface Props {
    children: React.ReactNode;
}

interface ServerToClientEvents {
    connection: string;
    noArg: () => void;
    basicEmit: (a: number, b: string, c: ArrayBuffer) => void;
    withAck: (d: string, callback: (e: number) => void) => void;
    connect: (callback: (e: object) => void) => void;
}

interface ClientToServerEvents {
    connection: string;
    userupdate: (callback: (e: object) => void) => void;
}



const SocketContext = createContext({});

const SocketIO = ({ children }: Props) => {
    const socket = useRef<Socket<ServerToClientEvents, ClientToServerEvents>>(io('http://192.168.0.107:3333'))
    useEffect(() => {
        socket.current.on('connect', () => {
            console.log('connected WS');
        });
        return () => {
            socket.current.off("connection")
        }
    }, []);
    return (
        <SocketContext.Provider value={socket}>{children}</SocketContext.Provider>
    )
};

export default SocketIO;
