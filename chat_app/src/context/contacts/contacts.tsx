import React, { createContext, useState } from 'react';
import Contacts from 'react-native-contacts';

interface ContactProps {
    name?: string;
    number?: string;
}


interface ContextProps {
    Contacts: Array<ContactProps> | null;
    SetContacts: React.Dispatch<React.SetStateAction<ContactProps[] | null>>
}

export const ContactsContext = createContext<ContextProps | null>(null)


interface Props {
    children: React.ReactNode;
}


const ContactProvider: React.FC<Props> = ({ children }: Props) => {

    let [Contacts, SetContacts] = useState<Array<ContactProps> | null>(null)

    return (
        <ContactsContext.Provider value={{ Contacts, SetContacts }}>
            {children}
        </ContactsContext.Provider>
    )
}


export default ContactProvider