import React from 'react';
import {
  FlatList,
  Image,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import { globalStyles } from '../../styles/globalstyles';
import FaIcon from 'react-native-vector-icons/Feather';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import TextComponent from '../../components/TextComponent';
import { fontFamilies, Messures } from '../../constants/constants';
import MessageItem from '../../components/message_item';
import { StackNavigationProp } from '@react-navigation/stack';
import { StackRootParamsList } from '../../routes/Stack/StackNavigator';

interface HomeProps {
  navigation: StackNavigationProp<StackRootParamsList, 'Home'>;
}

const Home: React.FC<HomeProps> = ({ navigation }: HomeProps) => {
  const openMessages = () => {
    navigation.navigate('MessageScreen');
  };

  return (
    <View style={globalStyles.main}>
      <View style={styles.header_profile}>
        <View>
          <View style={styles.heading_text_wrapper}>
            <TextComponent styles={styles.person_name}>
              Hello, Rohith
            </TextComponent>
          </View>
          <View style={styles.heading_text_wrapper}>
            <TextComponent styles={styles.heading_text}>
              Chat message
            </TextComponent>
          </View>
        </View>
        <View>
          <TouchableOpacity style={globalStyles.icon_wrapper}>
            <FaIcon name={'edit'} size={24} color={Colors.black} />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.horizontal_scroll_wrapper}>
        <FlatList
          showsVerticalScrollIndicator={false}
          showsHorizontalScrollIndicator={false}
          data={Array(20)}
          contentContainerStyle={styles.pics_wrapper}
          horizontal
          renderItem={({ item, index }) => (
            <View style={styles.feed_wrapper}>
              <TouchableOpacity style={styles.circle_person}>
                <Image
                  source={{ uri: 'https://picsum.photos/200/300' }}
                  resizeMode={'cover'}
                  style={styles.pics}
                />
              </TouchableOpacity>
              <TextComponent>Rohith</TextComponent>
            </View>
          )}
        />
      </View>
      <View style={styles.messages_wrapper}>
        <FlatList
          style={styles.messages_style}
          data={Array(50)}
          contentContainerStyle={styles.messages_list}
          renderItem={({ item, index }) => (
            <MessageItem
              onPress={() => openMessages()}
              message_text="Rohith dhajshdkj aj ja jksahj hjash jhdasjh jkdh"
              message_description="Hi Rohith dysuaiduas udauis uduisag uidsaui gudas gdi gasgid guag uigag dgi guasgu guasd"
            />
          )}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header_profile: {
    width: '100%',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: Messures.padding,
    paddingVertical: 16,
  },
  heading_text_wrapper: {
    paddingVertical: 2,
  },
  person_name: {
    fontSize: 16,
  },
  heading_text: {
    fontSize: 22,
    fontFamily: fontFamilies.kalam._700,
  },
  horizontal_scroll_wrapper: {
    width: '100%',
    paddingVertical: 4,
    borderBottomWidth: 1,
    borderBottomColor: 'rgba(0,0,0,0.4)',
  },
  pics_wrapper: {
    height: '100%',
    alignItems: 'center',
  },
  feed_wrapper: {
    alignItems: 'center',
  },
  circle_person: {
    height: 60,
    width: 60,
    borderRadius: 30,
    borderWidth: 1,
    overflow: 'hidden',
    justifyContent: 'center',
    marginHorizontal: Messures.padding,
    alignItems: 'center',
  },
  pics: {
    width: 58,
    padding: 2,
    height: 58,
    borderRadius: 58 / 2,
  },
  messages_wrapper: {
    flex: 1,
  },
  messages_style: {
    flex: 1,
  },
  messages_list: {
    paddingTop: 24,
    // paddingHorizontal: Messures.padding
  },
});

export default Home;
