import React from 'react';
import {
  FlatList,
  Image,
  ImageBackground,
  StyleSheet,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import { globalStyles } from '../../styles/globalstyles';
import MessagesHeader from '../../components/message_header';
import ChatItem from '../../components/ChatItem';
import { Colors, fontFamilies } from '../../constants/constants';
import FeIcon from 'react-native-vector-icons/Feather';
import EaIcon from 'react-native-vector-icons/Entypo';

const MessageScreen = () => {
  return (
    <View style={globalStyles.main}>
      <MessagesHeader />
      <ImageBackground
        source={{
          uri: 'https://images.unsplash.com/photo-1657299143322-934f44698807?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=687&q=80',
        }}
        style={styles.background_image}>
        <FlatList
          style={styles.message_wrapper}
          data={Array(20)}
          renderItem={({ item, index }) => (
            <>
              {index % 2 ? (
                <ChatItem
                  isLeft={true}
                  message={`import EnTypeIcon from "react-native-vector-icons/Entypo"
import FeIcon from "react-native-vector-icons/Feather"
import { Colors, fontFamilies, Messures } from "../../constants/constants";`}
                />
              ) : (
                <ChatItem
                  message={`import EnTypeIcon from "react-native-vector-icons/Entypo"
import FeIcon from "react-native-vector-icons/Feather"
import { Colors, fontFamilies, Messures } from "../../constants/constants";`}
                />
              )}
            </>
          )}
        />
        <InputWrapper />
      </ImageBackground>
    </View>
  );
};

const InputWrapper: React.FC = () => {
  return (
    <View style={styles.chat_area_wrapper}>
      <View style={styles.chat_area_input_wrapper}>
        <View style={styles.chat_input_wrapper}>
          <TextInput
            style={styles.input_field}
            multiline
            placeholder="message..."
            placeholderTextColor={'rgba(0,0,0,0.75)'}
          />
        </View>
        <View style={styles.chat_area_input_icons_wrapper}>
          <TouchableOpacity style={styles.chat_area_input_icon_wrapper}>
            <EaIcon name="attachment" color={Colors.black} size={24} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.chat_area_input_icon_wrapper}>
            <FeIcon name="camera" color={Colors.black} size={24} />
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.chat_area_send_icon_wrapper}>
        <TouchableOpacity style={styles.chat_area_icon_wrapper}>
          <FeIcon name="send" color={Colors.black} size={24} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  message_wrapper: {
    flex: 1,
    backgroundColor: 'transparent',
  },
  chat_area_wrapper: {
    width: '100%',
    minHeight: 50,
    paddingVertical: 4,
    paddingHorizontal: 8,
    flexDirection: 'row',
    backgroundColor: 'transparent',
    alignItems: 'flex-end',
  },
  chat_area_input_wrapper: {
    borderWidth: 1,
    flex: 1,
    borderRadius: 20,
    maxHeight: 150,
    flexDirection: 'row',
    paddingRight: 12,
    backgroundColor: '#fff',
  },
  chat_input_wrapper: {
    flex: 1,
    paddingHorizontal: 8,
    justifyContent: 'center',
  },
  input_field: {
    minHeight: 30,
    borderRadius: 20,
    color: '#000',
    fontFamily: fontFamilies.kalam._400,
    fontSize: 12,
    padding: 4,
    paddingHorizontal: 12,
  },
  chat_area_input_icons_wrapper: {
    height: 40,
    alignSelf: 'flex-end',
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  chat_area_input_icon_wrapper: {
    width: 30,
    height: 30,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 2,
  },
  chat_area_send_icon_wrapper: {
    borderWidth: 1,
    width: 40,
    height: 40,
    borderRadius: 20,
    marginLeft: 4,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  chat_area_icon_wrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
    width: 30,
    borderRadius: 15,
  },
  background_image: {
    flex: 1,
  },
});

export default MessageScreen;
