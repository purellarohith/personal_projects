import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import Home from '../../screens/home/home';
import MessageScreen from '../../screens/message_screen/message_screen';

export type StackRootParamsList = {
  Home: undefined;
  MessageScreen: undefined;
};

const Stack = createStackNavigator<StackRootParamsList>();

const StackNavigation = () => {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen name="Home" component={Home} />
      <Stack.Screen name="MessageScreen" component={MessageScreen} />
    </Stack.Navigator>
  );
};

export default StackNavigation;
