import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import StackNavigation from './Stack/StackNavigator';

const Route: React.FC = () => {
  return (
    <NavigationContainer>
      <StackNavigation />
    </NavigationContainer>
  );
};

export default Route;
