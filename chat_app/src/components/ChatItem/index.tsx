import React from 'react';
import { StyleSheet, View } from 'react-native';
import TextComponent from '../TextComponent';

interface ChatItemProps {
  isLeft?: boolean;
  message: string;
}

const ChatItem: React.FC<ChatItemProps> = ({
  isLeft = false,
  message,
}: ChatItemProps) => {
  return (
    <View style={isLeft ? styles.left_render : styles.right_render}>
      <TextComponent>{message}</TextComponent>
      <View style={isLeft ? styles.left_before : styles.right_after} />
    </View>
  );
};

const styles = StyleSheet.create({
  left_render: {
    minHeight: 50,
    width: 250,
    backgroundColor: 'pink',
    marginVertical: 5,
    borderRadius: 6,
    marginHorizontal: 4,
    position: 'relative',
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  right_render: {
    position: 'relative',
    minHeight: 50,
    width: 250,
    marginVertical: 5,
    backgroundColor: 'green',
    alignSelf: 'flex-end',
    borderRadius: 6,
    marginHorizontal: 4,
    paddingHorizontal: 8,
    paddingVertical: 8,
  },
  left_before: {
    position: 'absolute',
    left: 0,
    bottom: -2,
    height: 12,
    backgroundColor: 'pink',
    width: 12,
    borderTopLeftRadius: 6,
    borderTopRightRadius: 12,
    borderBottomRightRadius: 12,
    zIndex: -1,
  },
  right_after: {
    position: 'absolute',
    right: 0,
    bottom: -1,
    height: 12,
    backgroundColor: 'green',
    width: 12,
    borderTopLeftRadius: 12,
    borderTopRightRadius: 6,
    borderBottomLeftRadius: 12,
    zIndex: -1,
  },
});

export default ChatItem;
