import React from 'react';
import {
  GestureResponderEvent,
  Image,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { Colors, fontFamilies, Messures } from '../../constants/constants';
import TextComponent from '../TextComponent';
import AntIcon from 'react-native-vector-icons/AntDesign';
import MatIcon from 'react-native-vector-icons/MaterialCommunityIcons';

interface messageProps {
  message_text: string;
  message_description: string;
  onPress?: ((event: GestureResponderEvent) => void) | undefined;
}

const MessageItem: React.FC<messageProps> = ({
  message_text,
  message_description,
  onPress,
}: messageProps) => {
  return (
    <TouchableOpacity style={styles.list_item_wrapper} onPress={onPress}>
      <View style={styles.left_container}>
        <TouchableOpacity style={styles.list_circle}>
          <Image
            source={{ uri: 'https://picsum.photos/200/300' }}
            resizeMode={'cover'}
            style={styles.pics}
          />
          <View style={styles.online} />
        </TouchableOpacity>
        <View style={styles.content_wrapper}>
          <View>
            <TextComponent
              props={{
                numberOfLines: 1,
                ellipsizeMode: 'tail',
              }}
              styles={styles.message_from}>
              {message_text}
            </TextComponent>
          </View>
          <View>
            <TextComponent
              props={{
                numberOfLines: 1,
              }}
              styles={styles.message_description}>
              {message_description}
            </TextComponent>
          </View>
        </View>
      </View>
      <View style={styles.right_wrapper}>
        <View>
          <TextComponent>02:00</TextComponent>
        </View>
        <View style={styles.notification_wrapper}>
          <TouchableOpacity style={styles.notification_container}>
            <AntIcon name="pushpin" size={16} color={Colors.black} />
          </TouchableOpacity>
          <TouchableOpacity style={styles.notification_container}>
            <MatIcon name="check-all" size={16} color={Colors.black} />
          </TouchableOpacity>
          <View style={styles.notification_number}>
            <TextComponent styles={styles.notification_number_text}>
              1
            </TextComponent>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  list_item_wrapper: {
    borderColor: '#000',
    paddingVertical: 8,
    flexDirection: 'row',
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
    paddingHorizontal: Messures.padding,
  },
  list_circle: {
    height: 60,
    width: 60,
    borderRadius: 30,
    position: 'relative',
    justifyContent: 'center',
    alignItems: 'center',
  },
  online: {
    position: 'absolute',
    bottom: 2,
    right: 8,
    height: 6,
    width: 6,
    borderRadius: 3,
    backgroundColor: 'green',
  },
  right_wrapper: {
    paddingHorizontal: 4,
    alignItems: 'center',
  },
  notification_wrapper: {
    flexDirection: 'row',
  },
  notification_number: {
    height: 18,
    width: 18,
    borderRadius: 9,
    borderColor: 'black',
    borderWidth: 1,
    backgroundColor: 'blue',
  },
  notification_number_text: {
    textAlign: 'center',
    textAlignVertical: 'center',
  },
  notification_container: {
    padding: 4,
  },
  left_container: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  content_wrapper: {
    flex: 1,
    paddingHorizontal: 8,
    marginHorizontal: 4,
  },
  message_from: {
    fontSize: 16,
    fontFamily: fontFamilies.kalam._700,
  },
  message_description: {
    fontSize: 12,
    fontFamily: fontFamilies.kalam._400,
  },
  pics: {
    width: 58,
    padding: 2,
    height: 58,
    borderRadius: 58 / 2,
  },
});

export default MessageItem;
