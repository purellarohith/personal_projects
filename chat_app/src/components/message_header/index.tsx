import React from 'react';
import { Image, StyleSheet, TouchableOpacity, View } from 'react-native';
import TextComponent from '../TextComponent';
import EnTypeIcon from 'react-native-vector-icons/Entypo';
import FeIcon from 'react-native-vector-icons/Feather';
import { Colors, fontFamilies, Messures } from '../../constants/constants';
import { globalStyles } from '../../styles/globalstyles';

const MessagesHeader = () => {
  return (
    <View style={styles.header_wrapper}>
      <View style={styles.header_left_wrapper}>
        <TouchableOpacity style={globalStyles.icon_wrapper}>
          <EnTypeIcon name="chevron-left" size={24} color={Colors.black} />
        </TouchableOpacity>
        <TouchableOpacity style={styles.profile_pic_wrapper}>
          <Image
            source={{ uri: 'https://picsum.photos/200/300' }}
            resizeMode={'cover'}
            style={styles.profile_pic}
          />
        </TouchableOpacity>
        <TouchableOpacity style={styles.profile_content_wrapper}>
          <View style={styles.profile_name_wrapper}>
            <TextComponent styles={styles.profile_name}>Rohith</TextComponent>
          </View>
          <View style={styles.profile_status_wrapper}>
            <TextComponent styles={styles.profile_status}>online</TextComponent>
          </View>
        </TouchableOpacity>
      </View>
      <View style={styles.header_right_wrapper}>
        <TouchableOpacity
          style={{
            ...globalStyles.icon_wrapper,
            ...styles.header_right_icons,
          }}>
          <FeIcon name="video" size={24} color={Colors.black} />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            ...globalStyles.icon_wrapper,
            ...styles.header_right_icons,
          }}>
          <FeIcon name="phone" size={24} color={Colors.black} />
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  header_wrapper: {
    flexDirection: 'row',
    paddingHorizontal: Messures.padding,
    paddingVertical: 12,
    justifyContent: 'space-between',
    alignItems: 'center',
    borderBottomColor: 'gray',
    borderBottomWidth: 1,
  },
  header_left_wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    flex: 1,
  },
  header_right_wrapper: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 4,
  },
  profile_pic_wrapper: {
    width: 40,
    height: 40,
    borderRadius: 20,
    borderWidth: 1,
    overflow: 'hidden',
    marginLeft: 12,
  },
  profile_name: {
    fontFamily: fontFamilies.kalam._700,
    fontSize: 16,
  },
  profile_status: {
    fontSize: 12,
  },
  profile_pic: {
    width: '100%',
    height: '100%',
  },
  profile_content_wrapper: {
    justifyContent: 'center',
    paddingHorizontal: 12,
    marginHorizontal: 8,
    flex: 1,
  },
  profile_name_wrapper: {},
  profile_status_wrapper: {},
  header_right_icons: {
    marginHorizontal: 4,
    padding: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default MessagesHeader;
