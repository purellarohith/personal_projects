import React from 'react';
import { StyleProp, Text, TextProps, TextStyle } from 'react-native';
import { fontFamilies } from '../../constants/constants';

interface TextProps_ {
  styles?: StyleProp<TextStyle>;
  children: React.ReactNode;
  props?: TextProps;
}

const TextComponent: React.FC<TextProps_> = ({
  styles,
  children,
  props,
}: TextProps_) => {
  return (
    <Text
      {...props}
      style={{
        color: 'black',
        fontFamily: fontFamilies.kalam._400,
        ...(styles as object),
      }}>
      {children}
    </Text>
  );
};

export default TextComponent;
