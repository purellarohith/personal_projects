export const Colors = {
  black: '#000',
};

export const Messures = {
  padding: 20,
};

export const fontFamilies = {
  kalam: {
    _100: 'light_kalam',
    _400: 'regular_kalam',
    _700: 'bold_kalam',
  },
};
