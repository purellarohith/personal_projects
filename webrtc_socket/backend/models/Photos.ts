import mongoose, { Document } from "mongoose";

interface PhotosInterface extends Document {
    paths: Array<{ uri: string }>
}

let photo_schema = new mongoose.Schema({
    uri: {
        type: String,
    }
}, {
    timestamps: true
})


let photos_schema = new mongoose.Schema({
    paths: {
        type: [photo_schema]
    }
}, {
    timestamps: true
})





const Photos = mongoose.model<PhotosInterface>('Photos', photos_schema)


export default Photos