import mongoose from 'mongoose';
import dotenv from 'dotenv';
dotenv.config();

interface mongodbCredentials {
    username?: string;
    password?: string;
    dbName: string;
    port?: number;
}

const DB_PORT = process.env.DB_PORT;
async function mongooseDatabaseConnect({ dbName, password, username, port = DB_PORT }: mongodbCredentials) {
    try {
        await mongoose.connect(`mongodb://${(username && password) ? `${username}:${password}@` : ''}localhost:${port}/${dbName}`, () => {
            console.log(`⚡️[MongoDB]: MongoDB is Connected!!!`)
        });
    } catch (error) {
        console.log(`⚡️[MongoDB]: Error on  MongoDB is connection!!!`)
    }
}


export default mongooseDatabaseConnect;