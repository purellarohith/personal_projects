import dotenv from 'dotenv';
import express, { Express } from 'express';
import { Server } from 'socket.io';
import bodyParser from './config/bodyParser';
import Router from './routes';
import cors from 'cors';
import mongoose from 'mongoose';
import mongooseDatabaseConnect from './config/mongodb';
import { createServer } from 'http'
import upload from './utils/ImageUploader';

dotenv.config();
mongooseDatabaseConnect({ dbName: 'web_rtc_chat_app' })

const port = process.env.PORT;
const app: Express = express();
const httpServer = createServer(app)
const IO = new Server(httpServer, {
    cors: {
        origin: '*',
    },
})


app.use([...bodyParser(express), upload, cors()])

app.use('/', Router);

let users = [];

IO.on('connection', (socket) => {
    console.log(`⚡️[Socket IO]: Socket User is Connected!!!`)

    socket.on('user', () => {
        console.log(`user`)
    })

    IO.on('disconnect', (socket) => {
        console.log(`⚡️[Socket IO]: Socket User is Disconnected!!!`)
    })
})

httpServer.listen(port, () => {
    console.log(`⚡️[Express]: Express is Connected to ${port}!!!`)
})

