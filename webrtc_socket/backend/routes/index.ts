import express, { Express } from 'express';
import * as core from 'express-serve-static-core';
import User from '../models/User';
import upload from '../utils/ImageUploader';

const Router: core.Router = express.Router();

Router.get('/', (req, res) => {
    res.send("⚡️[Server]: Server is Connected!!!")
});

Router.post('/user/signup', upload, async (req: core.Request, res: core.Response) => {
    let images = await req.files
    let user = await User.create(req.body)
    await user.encryptPassword(req.body.encrypt_password)
    user.save()
    res.send(user)
});
Router.post('/user/login', async (req: core.Request, res: core.Response) => {


    let user = await User.findOne({ email: req.body.email })
    if (!user) return res.send({ error: 0 })
    let result = await user.comparePassword(req.body.encrypt_password)
    let token = await user.createJwt(user?._id)
    let isVerified = await user.verifyJwt(token)
    user.encrypt_password = ""
    res.send({
        user,
        result,
        token,
        isVerified
    })
});


export default Router