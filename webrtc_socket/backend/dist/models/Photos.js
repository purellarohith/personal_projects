"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
let photo_schema = new mongoose_1.default.Schema({
    uri: {
        type: String,
    }
}, {
    timestamps: true
});
let photos_schema = new mongoose_1.default.Schema({
    paths: {
        type: [photo_schema]
    }
}, {
    timestamps: true
});
const Photos = mongoose_1.default.model('Photos', photos_schema);
exports.default = Photos;
