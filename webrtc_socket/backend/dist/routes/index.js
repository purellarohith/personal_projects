"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const User_1 = __importDefault(require("../models/User"));
const ImageUploader_1 = __importDefault(require("../utils/ImageUploader"));
const Router = express_1.default.Router();
Router.get('/', (req, res) => {
    res.send("⚡️[Server]: Server is Connected!!!");
});
Router.post('/user/signup', ImageUploader_1.default, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let images = yield req.files;
    let user = yield User_1.default.create(req.body);
    yield user.encryptPassword(req.body.encrypt_password);
    user.save();
    res.send(user);
}));
Router.post('/user/login', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    let user = yield User_1.default.findOne({ email: req.body.email });
    if (!user)
        return res.send({ error: 0 });
    let result = yield user.comparePassword(req.body.encrypt_password);
    let token = yield user.createJwt(user === null || user === void 0 ? void 0 : user._id);
    let isVerified = yield user.verifyJwt(token);
    user.encrypt_password = "";
    res.send({
        user,
        result,
        token,
        isVerified
    });
}));
exports.default = Router;
