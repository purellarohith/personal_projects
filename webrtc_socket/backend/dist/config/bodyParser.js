"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = (express) => [
    express.json(),
    express.urlencoded({ extended: false }),
];
exports.default = bodyParser;
