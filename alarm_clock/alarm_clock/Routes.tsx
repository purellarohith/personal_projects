import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator, StackNavigationProp } from "@react-navigation/stack";
import React, { useState } from "react";
import Home from "./src/screens/Home";
import Settings from "./src/screens/Settings";
import FeIcon from 'react-native-vector-icons/Feather'

export type StackRootParamsList = {
    BottomTab: undefined;
}

export type BottomRootParamsList = {
    Home: undefined;
    Settings: undefined;
}

const Routes: React.FC = () => {

    const Stack = createStackNavigator<StackRootParamsList>()

    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={{
                    headerShown: false
                }}
            >
                <Stack.Screen name="BottomTab" component={BottomTab} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}


interface props {
    navigation: StackNavigationProp<StackRootParamsList, 'BottomTab'>;
}

const BottomTab: React.FC<props> = ({ navigation }: props) => {



    const BottomNavigation = createBottomTabNavigator<BottomRootParamsList>()
    return (
        <BottomNavigation.Navigator
            screenOptions={{
                headerShown: false,
                tabBarInactiveTintColor: 'gray',
            }}
        >
            <BottomNavigation.Screen
                name="Home"
                component={Home}
                options={{
                    tabBarIcon: ({ color, focused, size }) => (
                        <FeIcon name="home" color={color} size={size} />
                    )
                }}
            />
            <BottomNavigation.Screen
                name="Settings"
                component={Settings}
                options={{
                    tabBarIcon: ({ color, focused, size }) => (
                        <FeIcon name="settings" color={color} size={size} />
                    )
                }}
            />

        </BottomNavigation.Navigator>
    )
}



export default Routes



