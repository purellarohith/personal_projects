import React from "react";
import Svg from "react-native-svg";
import { Dimensions } from "react-native";
const { width } = Dimensions.get("window");

const Clock: React.FC = () => {

    return <Svg height={width} width={width}></Svg>;
};

export default Clock;