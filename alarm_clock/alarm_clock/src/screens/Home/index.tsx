import { BottomTabScreenProps } from '@react-navigation/bottom-tabs';
import { CompositeScreenProps } from '@react-navigation/native';
import { StackScreenProps } from '@react-navigation/stack';
import React from 'react';
import { ScrollView, Text, useColorScheme, View } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { BottomRootParamsList, StackRootParamsList } from '../../../Routes';
import Clock from '../../components/clock';
import { globalStyles } from '../../styles/global';



type HomeScreenNavigationProp = CompositeScreenProps<
    BottomTabScreenProps<BottomRootParamsList, 'Home'>,
    StackScreenProps<StackRootParamsList>
>;


const Home: React.FC<HomeScreenNavigationProp> = ({ navigation, route }: HomeScreenNavigationProp) => {

    const isDarkMode = useColorScheme() === 'dark';

    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    };


    return (
        <View style={globalStyles.full_view}>
            <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', flex: 1, height: '100%' }}>
                <Clock />
            </View>
        </View>
    )
}


export default Home



