import { BottomTabScreenProps } from '@react-navigation/bottom-tabs';
import { CompositeScreenProps } from '@react-navigation/native';
import { StackScreenProps } from '@react-navigation/stack';
import React from 'react';
import { ScrollView, Text, useColorScheme, View } from 'react-native';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { BottomRootParamsList, StackRootParamsList } from '../../../Routes';



export type SettingsScreenNavigationProp = CompositeScreenProps<
    BottomTabScreenProps<BottomRootParamsList, 'Settings'>,
    StackScreenProps<StackRootParamsList>
>;


const Settings: React.FC<SettingsScreenNavigationProp> = ({ navigation, route }: SettingsScreenNavigationProp) => {

    const isDarkMode = useColorScheme() === 'dark';

    const backgroundStyle = {
        backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
    };


    return (
        <View>
            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={backgroundStyle}>


            </ScrollView>
        </View>
    )
}


export default Settings