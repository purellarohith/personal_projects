import { StyleSheet } from "react-native";


export const globalStyles = StyleSheet.create({
    full_view: {
        display: 'flex',
        flex: 1,
        height: '100%',
        width: '100%'
    }
}) 