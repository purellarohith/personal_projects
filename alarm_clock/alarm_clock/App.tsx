/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { type PropsWithChildren } from 'react';
import {
    SafeAreaView,
    StatusBar,
    StyleProp,
    StyleSheet,
    useColorScheme,
    ViewStyle,
} from 'react-native';

import {
    Colors,
} from 'react-native/Libraries/NewAppScreen';
import Routes from './Routes';



const App = () => {
    const isDarkMode = useColorScheme() === 'dark';

    const backgroundStyle: ViewStyle = {
        backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
        display: 'flex',
        flex: 1
    };

    return (
        <SafeAreaView style={backgroundStyle}>
            <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
            <Routes />
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    sectionContainer: {
        marginTop: 32,
        paddingHorizontal: 24,
    },
    sectionTitle: {
        fontSize: 24,
        fontWeight: '600',
    },
    sectionDescription: {
        marginTop: 8,
        fontSize: 18,
        fontWeight: '400',
    },
    highlight: {
        fontWeight: '700',
    },
});

export default App;





