"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const express_1 = __importDefault(require("express"));
const socket_io_1 = require("socket.io");
const bodyParser_1 = __importDefault(require("./config/bodyParser"));
const routes_1 = __importDefault(require("./routes"));
const cors_1 = __importDefault(require("cors"));
const mongodb_1 = __importDefault(require("./config/mongodb"));
const http_1 = require("http");
const ImageUploader_1 = __importDefault(require("./utils/ImageUploader"));
dotenv_1.default.config();
(0, mongodb_1.default)({ dbName: 'chat_app' });
const port = process.env.PORT;
const app = (0, express_1.default)();
const httpServer = (0, http_1.createServer)(app);
const IO = new socket_io_1.Server(httpServer, {
    cors: {
        origin: '*',
    },
});
app.use([...(0, bodyParser_1.default)(express_1.default), ImageUploader_1.default, (0, cors_1.default)()]);
app.use('/', routes_1.default);
let users = [];
IO.on('connection', (socket) => {
    console.log(`⚡️[Socket IO]: Socket User is Connected!!!`);
    socket.on('user', () => {
        console.log(`user`);
    });
    IO.on('disconnect', (socket) => {
        console.log(`⚡️[Socket IO]: Socket User is Disconnected!!!`);
    });
});
httpServer.listen(port, () => {
    console.log(`⚡️[Express]: Express is Connected to ${port}!!!`);
});
// {
//   "name":"as",
//   "email":"s@gmail.com",
//   "encrypt_password":"123456789",
//   "phone":"7984651230"
// }
