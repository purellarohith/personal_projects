"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
let user_schema = new mongoose_1.default.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    email: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    user_id: {
        type: String,
        default: Date.now(),
        immutable: true
    },
    encrypt_password: {
        type: String,
        required: true
    },
    photos: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        ref: 'Photos'
    }
}, {
    timestamps: true
});
user_schema.methods.encryptPassword = function (password) {
    return __awaiter(this, void 0, void 0, function* () {
        let salt = yield bcrypt_1.default.genSalt(16);
        this.encrypt_password = yield bcrypt_1.default.hash(password, salt);
        return true;
    });
};
user_schema.methods.comparePassword = function (password) {
    return __awaiter(this, void 0, void 0, function* () {
        let Auth = yield bcrypt_1.default.compare(password, this.encrypt_password);
        return Auth;
    });
};
user_schema.methods.createJwt = function (userId) {
    return __awaiter(this, void 0, void 0, function* () {
        let Token = yield jsonwebtoken_1.default.sign({ userId }, process.env.JWT_SECURE, { expiresIn: '5d' });
        return Token;
    });
};
user_schema.methods.verifyJwt = function (token) {
    return __awaiter(this, void 0, void 0, function* () {
        let isVerifiedToken = yield jsonwebtoken_1.default.verify(token, process.env.JWT_SECURE);
        return isVerifiedToken;
    });
};
const User = mongoose_1.default.model('Users', user_schema);
exports.default = User;
