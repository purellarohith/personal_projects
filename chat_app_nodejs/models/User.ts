import mongoose, { Document } from "mongoose";
import bcrypt from 'bcrypt';
import JWT from 'jsonwebtoken';
import dotenv from "dotenv"
dotenv.config()

interface UserInterface extends Document {
    name: string
    email: string
    phone: string
    user_id: string
    encrypt_password: string
    photos: string
    encryptPassword(password: string): boolean
    comparePassword(password: string): boolean
    createJwt(userId: string): string
    verifyJwt(token: string): boolean
}

let user_schema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        required: true
    },
    email: {
        type: String,
        trim: true,
        required: true,
        unique: true
    },
    phone: {
        type: String,
        required: true,
        unique: true
    },
    user_id: {
        type: String,
        default: Date.now(),
        immutable: true
    },
    encrypt_password: {
        type: String,
        required: true
    },
    photos: {
        type: mongoose.SchemaTypes.ObjectId,
        ref: 'Photos'
    }

}, {
    timestamps: true
})



user_schema.methods.encryptPassword = async function (password: string) {
    let salt = await bcrypt.genSalt(16);
    this.encrypt_password = await bcrypt.hash(password, salt)
    return true
}

user_schema.methods.comparePassword = async function (password: string) {
    let Auth = await bcrypt.compare(password, this.encrypt_password)
    return Auth
}

user_schema.methods.createJwt = async function (userId: string) {
    let Token = await JWT.sign({ userId }, process.env.JWT_SECURE, { expiresIn: '5d' })
    return Token
}
user_schema.methods.verifyJwt = async function (token: string) {
    let isVerifiedToken = await JWT.verify(token, process.env.JWT_SECURE)
    return isVerifiedToken
}

const User = mongoose.model<UserInterface>('Users', user_schema)


export default User