import multer from "multer";
import dotenv from 'dotenv'
dotenv.config()



const storage = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, process.env.DRIVE_PATH)
    },
    filename: (req, file, callback) => {
        callback(null, `${Date.now()}${file.originalname}`)
    }
})

const upload = multer({ storage: storage });



export default upload.any()