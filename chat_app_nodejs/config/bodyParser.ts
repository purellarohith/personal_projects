

const bodyParser = (express: any) => [
    express.json(),
    express.urlencoded({ extended: false }),

]




export default bodyParser;