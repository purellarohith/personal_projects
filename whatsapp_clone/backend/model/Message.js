import mongoose from "mongoose";

let message_schema = new mongoose.Schema({
    conversationId: {
        type: String
    },
    senderId: {
        type: String
    },
    receiverId: {
        type: String
    },
    type: {
        type: String
    },
    value: {
        type: String
    }
}, {
    timestamps: true
})




const Messages = mongoose.model('messages', message_schema)


export default Messages