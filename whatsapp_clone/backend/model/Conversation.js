import mongoose from "mongoose";

let conversation_schema = new mongoose.Schema({
    members: {
        type: Array
    },
    message: {
        type: String
    }
}, {
    timestamps: true
})


const Conversation = mongoose.model('user_conversations', conversation_schema);

export default Conversation