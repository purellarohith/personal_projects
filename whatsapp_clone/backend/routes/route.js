import express from 'express';
import { newConversation } from '../controller/conversation.js';
import { getMessages, newMessage } from '../controller/message.js';
import { addUser, getUsers } from '../controller/user.js';

const router = express.Router();



router.post('/add', addUser)
router.get('/allusers', getUsers)
router.post('/conversation/add', newConversation)
router.post('/message/add', newMessage)
router.get('/message/get/:Id', getMessages)



export default router