import mongoose from "mongoose"

// whatsapp_mongo_0.0.1

const Connection = async () => {
    let url = 'mongodb://localhost:27017/whatsapp_mongo'
    try {
        await mongoose.connect(url, () => {
            console.log('connected to mongodb!')
        })
    } catch (error) {
        console.log('not connected to mongodb!')
    }
}


export default Connection