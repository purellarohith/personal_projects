import Conversation from "../model/Conversation.js"
import Messages from "../model/Message.js"

export const newMessage = async (req, res) => {
    let {
        senderId,
        receiverId,
        conversationId,
        type,
        value
    } = req.body
    try {
        let newmessage = await new Messages(req.body)
        await newmessage.save()
        await Conversation.findByIdAndUpdate(conversationId, { message: value })
        res.send({ success: 1, message: 'messgae sent successfully' })
        res.end()
    } catch (error) {
        console.log(error)
        res.status(404)
        res.send({ success: 0 })
        res.end()
    }
}

export const getMessages = async (req, res) => {
    let { Id } = req.params
    try {
        let all_messages = await Messages.find({ conversationId: Id })
        res.send({ success: 1, message: 'fetched all messages successfully', all_messages })
        res.end()
    } catch (error) {
        console.log(error)
        res.status(404)
        res.send({ success: 0 })
        res.end()
    }
}