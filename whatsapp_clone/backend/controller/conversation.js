import Conversation from "../model/Conversation.js"

export const newConversation = async (req, res) => {
    try {
        let { senderId, receiverId } = req.body

        const exist = await Conversation.findOne({ members: { $all: [receiverId, senderId] } })
        if (exist) {
            res.status(200)
            res.send({ success: 0, messgae: 'already user coversation exits', conversationId: exist._id })
            res.end()
            return
        }


        const newConversation = new Conversation({
            members: [
                receiverId, senderId
            ]
        })

        let conversation = await newConversation.save()
        res.send({ success: 1, message: 'successfully saved', conversationId: conversation._id })
        res.end()
    } catch (error) {
        console.log(error)
        res.status(404)
        res.send({ success: 0 })
        res.end()
    }
}