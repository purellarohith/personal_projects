import User from "../model/User.js"


export const addUser = async (req, res) => {
    let { name, email_verified, email } = req.body

    try {
        let userExits = await User.findOne({ sub: req.body.sub })
        if (userExits) return res.send({ success: 0 })
        let addUser = new User(req.body)
        await addUser.save()
        res.send({ name: name, email: email, success: 1, msg: 'added user' })
        res.end()
    } catch (error) {
        console.log(error)
        res.status(404)
        res.end()
    }
}




export const getUsers = async (req, res) => {

    try {
        let users = await User.find({})
        res.send({ users: users, success: 1 })
        res.end()
    } catch (error) {
        console.log(error)
        res.status(404)
        res.send({ success: 0 })
        res.end()
    }
}