import express from 'express'
import Connection from './database/bd.js';
import router from './routes/route.js';
import cors from 'cors';
import { createServer } from 'http'
import { Server } from 'socket.io'

const app = express();
let httpserver = createServer(app)
const port = 3333;
const io = new Server(httpserver, {
    cors: {
        origin: '*'
        // origin: 'http://localhost:3000'
    }
});

let users = []

// DB_Connection
Connection()

// Middelware
app.use(cors())
app.use([express.json(), express.urlencoded({ extended: true })])

// Routes
app.use('/', router)



// Socket Functions

let adduser = (userdata, id) => {
    !users.some(user => user?.sub == userdata.sub) && users.push({ ...userdata, id })
}

let getUser = (userId) => {
    return users.find(user => user.sub === userId)
}


let disconnectUser = (userId) => {
    let copy_users = [...users]
    let remain_users = copy_users.filter(user => user.id !== userId)
    users = remain_users
    return users
}

// Socket
io.on("connection", (socket) => {
    socket.on('addusers', data => {
        adduser(data, socket.id)
        io.emit('getactiveusers', users)
    })

    socket.on('sendMessages', data => {
        const user = getUser(data.receiverId)
        if (user) {
            io.to(user.id).emit('getMessages', data)
        }
    })

    socket.on('disconnect', data => {
        disconnectUser(socket.id)
        io.emit('getactiveusers', users)
    })

});

httpserver.listen(port, () => {
    console.log(`app running successfully ${port}`)
})
