import { GoogleOAuthProvider } from '@react-oauth/google';
import './App.css';
import Messagenger from './components/Messanger';
import AccontProvider from './context/AccontProvider';

function App() {
  const clientId = '533685038865-c78o2rgc3hcakl9rrt2880la124h3b6.apps.googleusercontent.com'


  return (
    <AccontProvider>
      <GoogleOAuthProvider clientId={clientId}>
        <Messagenger />
      </GoogleOAuthProvider>
    </AccontProvider>

  );
}

export default App;
