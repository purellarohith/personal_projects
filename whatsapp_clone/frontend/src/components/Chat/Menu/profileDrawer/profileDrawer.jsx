import { ArrowBack } from '@mui/icons-material'
import { Box, Drawer, styled, Typography } from '@mui/material'
import React from 'react'
import Profile from './Profile/Profile'



const drawerStyle = {
    left: 20,
    top: 22,
    height: '95%',
    width: '30%',
    minWidth: "450px",
    boxShadow: 'none',
}


const Header = styled(Box)`
    background-color: #008069;
    height: 107px;
    color: #ffffff;
    display: flex;
    & > svg , & > p {
        margin-top: auto;
        padding: 15px;
        font-weight: 600;

    }
`
const Component = styled(Box)`
    background-color: #ededed;
    height: 100%;
`

const Text = styled(Typography)`
    font-size: 18px;
`

const ProfileDrawer = ({ open, setOpen }) => {
    const closeHandler = () => {
        setOpen(false)
    }
    return (
        <Drawer
            open={open}
            onClose={closeHandler}
            PaperProps={{ sx: drawerStyle }}
            style={{ zIndex: 1500 }}
        >
            <Header>
                <ArrowBack onClick={() => setOpen(false)} />
                <Text>Profile</Text>
            </Header>
            <Component>
                <Profile />
            </Component>
        </Drawer>
    )
}

export default ProfileDrawer
