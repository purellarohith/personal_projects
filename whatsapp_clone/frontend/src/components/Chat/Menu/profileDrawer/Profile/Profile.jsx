import { Box, styled, Typography } from '@mui/material'
import { borderRadius } from '@mui/system'
import React, { useContext } from 'react'
import { AccontContext } from '../../../../../context/AccontProvider'




const ImageComponent = styled(Box)`
    display: flex;
    justify-content: center;
`

const Image = styled('img')({
    width: 200,
    height: 200,
    borderRadius: '50%',
    padding: "25px 0",


})


const BoxWrapper = styled(Box)`
    background: #fff;
    padding: 12px 30px 2px;
    box-shadow: 0 1px 3px rgba(0,0,0,0.08);
    & :first-child{
        font-size: 13px;
        color: #009688;
        font-weight: 200;
    }
    & :last-child{
        font-size: 13px;
        color: #4a4a4a;
        margin: 14px 0;
    }
`

const DescriptionContainer = styled(Box)`
    padding: 15px 20px 28px 30px;
    & > p{
        font-size: 13px;
        color: #8696a0;
    }
`

const Profile = () => {

    const { accont, setAccont } = useContext(AccontContext)

    return (
        <>
            <ImageComponent>
                <Image src={accont.picture} alt='dp' />
            </ImageComponent>
            <BoxWrapper>
                <Typography>Your name</Typography>
                <Typography>{accont.name}</Typography>
            </BoxWrapper>
            <DescriptionContainer>
                <Typography>
                    This is not your username or pin. This name will be visible to your WhatsApp contacts.
                </Typography>
            </DescriptionContainer>
            <BoxWrapper>
                <Typography>About</Typography>
                <Typography>Hahahaha...</Typography>
            </BoxWrapper>
        </>
    )

}

export default Profile
