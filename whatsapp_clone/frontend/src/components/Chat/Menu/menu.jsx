import { Box } from "@mui/material"
import { useState } from "react"
import Chat from "./ChatComponents/Chat"
import MenuHeader from "./menuHeader/MenuHeader"
import SearchComponent from "./search/search"

const Menu = () => {

    const [searchText, SetSearchText] = useState('')

    return (
        <Box>
            <MenuHeader />
            <SearchComponent
                {...{
                    searchText,
                    SetSearchText
                }}
            />
            <Chat
                {...{
                    searchText
                }}
            />
        </Box>
    )
}

export default Menu