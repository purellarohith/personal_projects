import { Box, styled, Typography } from '@mui/material'
import React, { useContext, useEffect } from 'react'
import { AccontContext } from '../../../../../context/AccontProvider'
import { setConversation } from '../../../../../hook/api'


let Component = styled(Box)`
    display: flex;
    height: 45px;
    padding:13px 0;
    cursor: pointer;
`
let Container = styled(Box)`

`
let ImageContainer = styled(Box)`

`
let Image = styled('img')({
    width: 50,
    height: 50,
    borderRadius: '50%',
    padding: '0 14px',
    objectFit: 'cover'
})

const OnlineStatus = styled(Box)`
    height: 12px;
    width: 12px;
    border-radius: 6px;
    position: absolute;
    right: 12px;
    bottom: -3px;
`

const ConversationItem = ({ user }) => {

    const { SetPerson, accont, ActiveUsers } = useContext(AccontContext)

    const getUser = async () => {
        let response = await setConversation({ senderId: accont.sub, receiverId: user.sub })
        SetPerson({ ...user, conversationId: response.conversationId })
    }


    return (
        <Component onClick={() => getUser()}>
            <Box style={{ position: 'relative' }}>
                <Image src={user.picture} alt={'user_image'} />
                <OnlineStatus style={{ background: ActiveUsers.find(active_user => active_user.sub == user.sub) ? 'green' : 'gray' }} />
            </Box>
            <Box>
                <Box>
                    <Typography>{user.name}</Typography>
                </Box>
            </Box>
        </Component>
    )
}

export default ConversationItem
