import { Box, Divider, styled } from '@mui/material';
import React, { useContext, useEffect, useState } from 'react';
import { getUsers } from '../../../../hook/api';
import ConversationItem from './Conversation/ConversationItem';
import { AccontContext } from './../../../../context/AccontProvider'

let Component = styled(Box)`
    /* height: 81vh; */
    overflow: overlay;
    
`
const DividerComponent = styled(Divider)`
    margin: 0 0 0 70px;
    background: #e9edef;
    opacity: .6;
`

const Chat = ({ searchText }) => {

    const [Users, SetUsers] = useState(null)
    const { accont, socket, ActiveUsers, SetActiveUsers } = useContext(AccontContext)
    useEffect(() => {
        init(searchText)
    }, [searchText])

    useEffect(() => {
        accont && socket.current?.emit('addusers', accont)
        accont && socket.current?.on('getactiveusers', activeUsers => {
            SetActiveUsers(activeUsers)
        })

        // return () => {
        //     // socket.cuurent.off('off')
        // }
    }, [accont])


    const init = async (search_text) => {
        let res = await getUsers()
        let finalUsers = await res.users.filter(v => v.sub !== accont.sub)
        let filter_users = await finalUsers.filter(user => user.name?.toLowerCase().includes(search_text?.toLowerCase()))
        SetUsers(filter_users)
    }


    return (
        <Component>
            {
                Users && Users.map((user, index) => (
                    <React.Fragment key={user._id}>

                        <ConversationItem
                            {...{
                                user
                            }}
                        />
                        <DividerComponent />
                    </React.Fragment>
                ))
            }
        </Component>
    )
}



export default Chat