import { Search } from "@mui/icons-material"
import { Box, InputBase } from "@mui/material"
import { styled } from "@mui/system"



const Component = styled(Box)`
    background-color: #fff;
    height: 45px;
    border-bottom: 1px solid #f2f2f2;
    display: flex;
    align-items: center;
`
const Wrapper = styled(Box)`
    background-color: #f0f2f5;
    position: relative;
    margin: 0 13px;
    width: 100%;
    border-radius: 10px;
`
const Icon = styled(Box)`
    position: absolute;
    height: 100%;
    padding: 6px 10px;
    color: #919191;
`

const InputWarpper = styled(InputBase)`
    width: 100%;
    padding: 16px;
    padding-left: 65px;
    height: 15px;
    font-size:14px ;
`

const SearchComponent = ({ searchText, SetSearchText }) => {
    return (
        <Component>
            <Wrapper>
                <Icon>
                    <Search
                        fontSize="small"
                    />
                </Icon>
                <InputWarpper
                    value={searchText}
                    placeholder="Search or Start"
                    onChange={(e) => SetSearchText(e.target.value)}
                />
            </Wrapper>
        </Component>
    )
}


export default SearchComponent