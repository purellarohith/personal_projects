import { MoreVert } from '@mui/icons-material'
import { Menu, MenuItem, styled } from '@mui/material'
import React, { useState } from 'react'



const MenuItemOption = styled(MenuItem)`
    font-size: 14px;
    padding: 15px 60px 5px 24px;
    color: #4a4a4a;
`



const HeaderDropDown = ({ openProfile, setOpenProfile }) => {

    const [open, setOpen] = useState(false)
    const clickHandler = (e) => {
        setOpen(e.currentTarget)
    }

    const closeHandler = () => {
        setOpen(false)
    }
    return (
        <>
            <MoreVert onClick={clickHandler} />
            <Menu
                anchorEl={open}
                open={open}
                keepMounted
                onClose={closeHandler}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center'
                }}
                MenuListProps={{
                    'aria-labelledby': 'basic-button'
                }}
                transformOrigin={{
                    horizontal: 'right',
                    vertical: 'top'
                }}
            >
                <MenuItemOption onClick={() => {
                    setOpenProfile(true)
                    setOpen(false)
                }}>Profile</MenuItemOption>
            </Menu>
        </>
    )
}


export default HeaderDropDown