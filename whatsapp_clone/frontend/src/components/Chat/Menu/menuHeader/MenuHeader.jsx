import { Box, styled } from "@mui/material"
import { useContext, useState } from "react"
import { AccontContext } from "../../../../context/AccontProvider"
import { Chat as ChatIcon } from '@mui/icons-material'
import HeaderDropDown from "./Headerdropdown"
import ProfileDrawer from "../profileDrawer/profileDrawer"


const Component = styled(Box)`
    height: 44px;
    background-color: #ededed;
    padding: 8px 16px;
    display: flex;
    align-items: center;
`


const Wrapper = styled(Box)`
    margin-left: auto;
    & > * {
        margin-left: 2px;
        padding: 8px;
        color: #000;
    }
    &:first-child{
        font-size: 22px;
        margin-right: 8px;
        margin-top: 3px;
    }
`


const CustomImage = styled('img')({
    height: 40,
    width: 40,
    borderRadius: '50%',
})

const MenuHeader = () => {
    const [openDrawer, setOpenDrawer] = useState(false)
    const { accont } = useContext(AccontContext)

    const toggleDrawer = () => {
        setOpenDrawer(!openDrawer)
    }

    return (
        <>
            <Component>
                <CustomImage src={accont.picture} alt={'pic'} onClick={() => setOpenDrawer(true)} />
                <Wrapper>
                    <ChatIcon />
                    <HeaderDropDown
                        {...{
                            openProfile: openDrawer,
                            setOpenProfile: setOpenDrawer
                        }}
                    />
                </Wrapper>
            </Component>
            <ProfileDrawer
                {...{
                    open: openDrawer,
                    setOpen: setOpenDrawer
                }}
            />
        </>
    )
}


export default MenuHeader