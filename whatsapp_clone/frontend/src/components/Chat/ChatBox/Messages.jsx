import { MoreVertOutlined, SearchOutlined } from '@mui/icons-material'
import { Box, styled, Typography } from '@mui/material'
import { DateTime } from 'luxon'
import React, { useContext, useEffect, useRef, useState } from 'react'
import { AccontContext } from '../../../context/AccontProvider'
import { getAllMessages, newMessage } from '../../../hook/api'
import Chatcontainer from './components/chatcontainer'
import Chatfooter from './components/chatfooter'



const BoxContainer = styled(Box)`
    background: #000;
    display: flex;
    height: 100%;
    flex-direction: column;
`

const HeaderContainer = styled(Box)`
    background: #f0f2f5;
    display: flex;
    height: 60px;
    max-height: 60px;
    align-items: center;
    padding: 10px 16px;
    justify-content: space-between;
`

const Image = styled('img')({
    height: 40,
    width: 40,
    borderRadius: '50%',
    objectFit: 'cover',
    overflow: 'hidden',
})

const UserName = styled(Typography)`
    margin-left: 15px;
    font-size: 16px;
    color: #111b21;
`

const LeftContainer = styled(Box)`
    display: flex;
    align-items: center;
`

const RightContainer = styled(Box)`
    display: flex;
    align-items: center;
    & > svg {
        padding-left: 8px;
        color: #000;
        font-size: 24px;
    }
`

const BodyWrapper = styled(Box)`
    display: flex;
    flex-direction: column;
    height: 100%;
    background-image: url('https://images.unsplash.com/photo-1542559272-819e1934ac51?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&q=80');
    background-size:cover;
    background-repeat: no-repeat;
`


const BoxBlock = styled(Box)`
    height: calc(100% - 60px);
    max-height: calc(100% - 60px);
`


const Messages = () => {

    const { Person, accont, socket } = useContext(AccontContext)
    const [ChatTextValue, SetChatTextValue] = useState('')
    const [Messages, SetMessages] = useState([])
    const scrollref = useRef()
    const [IncomingMessage, SetIncomingMessage] = useState(null)

    useEffect(() => {
        fetchMessage()
    }, [Person])


    useEffect(() => {
        Messages.length && scrollref.current?.scrollIntoView({ behavior: 'smooth' })
    }, [Messages])


    useEffect(() => {
        accont && socket.current.on('getMessages', (newmessages) => {
            newmessages && SetIncomingMessage({
                ...newmessages,
                createdAt: DateTime.now().toISO()
            })
        })
    }, [accont])


    useEffect(() => {
        if (IncomingMessage && Person.sub === IncomingMessage?.senderId) {
            SetMessages(PrevMessages => [...PrevMessages, IncomingMessage])
            SetIncomingMessage(null)
        }
    }, [IncomingMessage])


    const sendMessage = async (e) => {
        let code = e.keyCode || e.which;

        if (code === 13) {
            let message = {
                senderId: accont.sub,
                receiverId: Person.sub,
                conversationId: Person.conversationId,
                type: "text",
                value: ChatTextValue
            }
            socket.current.emit('sendMessages', message)
            await newMessage(message)
            SetMessages(PrevMessages => [...PrevMessages, {
                ...message, createdAt: DateTime.now().toISO()
            }])
            SetChatTextValue('')
        }
    }


    const fetchMessage = async () => {
        let response = await getAllMessages(Person.conversationId);
        SetMessages(response.all_messages)
    }


    return (
        <BoxContainer >
            <HeaderContainer>
                <LeftContainer>
                    <Image src={Person.picture} alt={'user_image'} />
                    <UserName>{Person.name}</UserName>
                </LeftContainer>
                <RightContainer>
                    <SearchOutlined />
                    <MoreVertOutlined />
                </RightContainer>
            </HeaderContainer>
            <BoxBlock>
                <BodyWrapper>
                    <Chatcontainer
                        {...{
                            Messages,
                            accont,
                            ref: scrollref
                        }}
                    />
                    <Chatfooter
                        {...{
                            ChatTextValue,
                            SetChatTextValue,
                            sendMessage
                        }}
                    />
                </BodyWrapper>
            </BoxBlock>
        </BoxContainer>
    )
}

export default Messages
