import { Box, InputBase, styled } from '@mui/material'
import React from 'react'
import { AttachFileOutlined, EmojiEmotionsOutlined, MicOutlined } from '@mui/icons-material'


const FooterContainer = styled(Box)`
    background: rgb(240,242,245);
    padding: 5px 17px 5px 10px ;
    display: flex;
    flex-direction: row;
    height: 60px;
`
const FooterLeftContainer = styled(Box)`
    display: flex;
    flex-direction: row;
    flex: 1;
    height: 100%;
`

const FooterRightContainer = styled(Box)`
    display: flex;
    flex-direction: row;
`

const SvgWrapper = styled(Box)`
    height: 100%;
    flex-direction: column;
    width: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
`

const TextContainer = styled(InputBase)`
    display: flex;
    flex: 1;
    background-color: #fff;
    padding: 9px 12px 11px;
    border-radius: 8px;
    height: 42px;
    align-self: center;
`

const Chatfooter = ({ ChatTextValue, SetChatTextValue, sendMessage }) => {

    return (
        <FooterContainer>
            <FooterLeftContainer>
                <SvgWrapper>
                    <EmojiEmotionsOutlined />
                </SvgWrapper>
                <SvgWrapper>
                    <label htmlFor='fileinput'>
                        <AttachFileOutlined />
                    </label>
                    <input
                        type={'file'}
                        id='fileinput'
                        style={{ display: 'none' }}
                    />
                </SvgWrapper>
                <TextContainer
                    value={ChatTextValue}
                    placeholder='Type a message'
                    onChange={(e) => SetChatTextValue(e.target.value)}
                    onKeyDown={(e) => sendMessage(e)}
                />
            </FooterLeftContainer>
            <FooterRightContainer>
                <SvgWrapper>
                    <MicOutlined />
                </SvgWrapper>
                {/* <SvgWrapper>
            <SendOutlined />
        </SvgWrapper> */}
            </FooterRightContainer>
        </FooterContainer>
    )
}

export default Chatfooter
