import { Box, styled, Typography } from '@mui/material'
import React from 'react'
import { DateTime } from 'luxon'



const BodyContainer = styled(Box)`
    display: flex;
    flex-direction: column;
    overflow-y: scroll;
    height: 100%; 
`


const LeftBox = styled(Box)`
    display: flex;
    align-items: center;
    justify-content: flex-start;
    width: 100%;
    padding: 22px 120px 22px 22px;
    `
const RightBox = styled(Box)`
    display: flex;
    align-items: center;
    justify-content: flex-end;
    padding: 22px  22px 22px 120px;
    width: 100%;
`

const LeftChatContainer = styled(Box)`
    background-color: #fff;
    position: relative;
    border-radius: 8px;
    padding: 6px 7px 8px 9px;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    min-width: 100px;
    max-width: 70%;
    /* align-self: flex-start; */
    min-height: 50px;
`

const LeftTail = styled(Box)`
    width: 0;
    height: 0;
    border-top: 12px solid #fff;
    border-left: 12px solid transparent;
    position: absolute;
    left: -12px;
    top: 0px;
    z-index: -12px;
`
const RightTail = styled(Box)`
    width: 0;
    height: 0;
    border-top: 12px solid #fff;
    border-right: 12px solid transparent;
    position: absolute;
    top: 0px;
    right: -12px;
    z-index: -12px;
`


const Time = styled(Typography)`
    color: #667781;
    font-size: 11px;
    text-align: end;
`

const ChatText = styled(Typography)`
    word-wrap: break-word;
`


const BoxWrapper = styled(Box)`
    display: flex;
    width: 100%;
`

const Chatcontainer = React.forwardRef(({ Messages, accont }, ref) => {
    return (
        <BodyContainer>
            {
                Messages.map((message, index) => (
                    <BoxWrapper key={message._id || index} ref={ref}>
                        {
                            accont.sub === message.senderId ?
                                <RightBox>
                                    <LeftChatContainer style={{ borderTopRightRadius: 0 }}>
                                        <RightTail />
                                        <ChatText>
                                            {
                                                message.value
                                            }
                                        </ChatText>
                                        <Time>{DateTime.fromISO(message.createdAt).toFormat('hh:mm a')}</Time>
                                    </LeftChatContainer>
                                </RightBox>
                                :
                                <LeftBox>
                                    <LeftChatContainer style={{ borderTopLeftRadius: 0 }}>
                                        <LeftTail />
                                        <ChatText>
                                            {
                                                message.value
                                            }
                                        </ChatText>
                                        <Time>{DateTime.fromISO(message.createdAt).toFormat('hh:mm a')}</Time>
                                    </LeftChatContainer>
                                </LeftBox>
                        }
                    </BoxWrapper>
                ))
            }
        </BodyContainer>
    )
})

export default Chatcontainer
