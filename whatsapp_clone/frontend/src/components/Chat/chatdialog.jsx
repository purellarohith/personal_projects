import { Box, Dialog, styled } from "@mui/material"
import { useContext } from "react"
import { AccontContext } from "../../context/AccontProvider"
import Messages from "./ChatBox/Messages"
import EmptyChat from "./emptyChat"
import Menu from "./Menu/menu"


const dialogStyle = {
    height: '95%',
    width: '100%',
    margin: '20px',
    maxHeight: '100%',
    maxWidth: '100%',
    borderRadius: '0',
    boxShadow: 'none',
    overflow: 'hidden',
    backgroundColor: 'none'
}


const Component = styled(Box)`
    display: flex;
    height: 100%;
`


const LeftComponent = styled(Box)`
    min-width: 450px;
    height: 100%;
    display: flex;
    flex-direction: column;
`

const RightComponent = styled(Box)`
    width: 100%;
    min-width: 300px;
    height: 100%;
    display: flex;
    flex-direction: column;
`

const ChatDialog = () => {
    const { Person } = useContext(AccontContext)
    return (
        <Dialog
            open
            PaperProps={{ sx: dialogStyle }}
            hideBackdrop
            maxWidth={'md'}
        >
            <Component>
                <LeftComponent>
                    <Menu />
                </LeftComponent>
                <RightComponent>
                    {
                        Object.keys(Person).length ?
                            <Messages />
                            :
                            <EmptyChat />
                    }
                </RightComponent>
            </Component>
        </Dialog>
    )
}


export default ChatDialog