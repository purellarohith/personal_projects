import { Box, styled, Typography } from "@mui/material"




const Component = styled(Box)`
    background: #f8f9fa;
    padding: 30px 0;
    text-align: center;
    height: 100%;
    display: flex;
    `

const Container = styled(Box)`
    padding: 200px 0;
    display: flex;
    flex: 1;
    justify-content: center;
    align-items: center;
`

const EmptyChat = () => {
    return (
        <Component>
            <Container>
                <Typography>There is no chats are avaliable...</Typography>
            </Container>
        </Component>
    )
}


export default EmptyChat