import { Box, Dialog, List, ListItem, styled, Typography } from "@mui/material";
import React, { useContext } from "react";
import { GoogleLogin } from '@react-oauth/google';
import JWTDecode from 'jwt-decode'
import { AccontContext } from "../../context/AccontProvider";
import { addUser } from "../../hook/api";

const Component = styled(Box)`
    display: flex;
    justify-content: space-around;
    
`
const Container = styled(Box)`
    padding: 56px 0 56px 56px;
`


const dialogStyle = {
    height: '96%',
    marginTop: '12%',
    width: '60%',
    maxHeight: '100%',
    maxWidth: '100%',
    boxShadow: 'none',
    overflow: 'none'
}


const QrCode = styled('img')({
    height: 264,
    width: 264,
    margin: '50px 0 0 50px',

})

const Title = styled(Typography)`
    font-size: 26px;
    color: #525252;
    font-weight: 300;
    font-family: inherit;
    margin-bottom: 25px;
`

const StyledList = styled(List)`
    & > li {
        padding: 0;
        margin-top: 15px;
        font-size: 16px;
        line-height: 26px;
    }
`

const LoginDialog = () => {


    const { setAccont } = useContext(AccontContext)



    const onLogin = (res) => {
        let decode_ = JWTDecode(res.credential)
        setAccont(decode_)
        addUser(decode_)
    }


    const onLoginError = (res) => {

    }

    return (
        <Dialog
            open
            PaperProps={{ sx: dialogStyle }}
            hideBackdrop
        >
            <Component>
                <Container>
                    <Title

                    >
                        To use WhatsApp on your computer:
                    </Title>
                    <StyledList>
                        <ListItem>
                            1. Open WhatsApp on your phone
                        </ListItem>
                        <ListItem>
                            2. Tap menu
                        </ListItem>
                        <ListItem>
                            3. Point your phone to this screen to capture the code
                        </ListItem>
                    </StyledList>
                </Container>
                <Box style={{ position: 'relative' }}>
                    <QrCode src="https://media.istockphoto.com/photos/close-up-of-qr-code-example-picture-id518484289" alt="qrcode" />
                    <Box
                        style={{
                            position: 'absolute',
                            top: '50%',
                            transform: 'translateX(50%)',
                        }}
                    >
                        <GoogleLogin
                            onSuccess={onLogin}
                            onError={onLoginError}
                        />
                    </Box>
                </Box>
            </Component>
        </Dialog>
    )

}


export default LoginDialog
