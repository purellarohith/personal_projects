import React, { useContext } from 'react';
import LoginDialog from './acconts/logindialog';
import { AppBar, Box, styled, Toolbar } from "@mui/material"
import { AccontContext } from '../context/AccontProvider';
import ChatDialog from './Chat/chatdialog';

const Component = styled(Box)`
    height: 100vh;
    background-color: #dcdcdc;
`

const Header = styled(AppBar)`
    height:125px;
    background-color: #00afa5;
    box-shadow: none;
`

const LoginHeader = styled(AppBar)`
    height:220px;
    background-color: #00bfa5;
    box-shadow: none;
`

const Messagenger = () => {
    const { accont, setAccont } = useContext(AccontContext);

    return (
        <Component>
            {accont ?
                <>
                    <Header >
                        <Toolbar>

                        </Toolbar>
                    </Header>
                    <ChatDialog />
                </>
                :
                <>
                    <LoginHeader >
                        <Toolbar>

                        </Toolbar>
                    </LoginHeader>
                    <LoginDialog />
                </>
            }
        </Component>
    )
}




export default Messagenger