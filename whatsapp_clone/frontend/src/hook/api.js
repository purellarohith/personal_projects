import axios from 'axios'

let url = 'http://localhost:3333'

export const addUser = async (data) => {
    try {
        let response = await axios.post(`${url}/add`, data)
        return response;

    } catch (error) {
        console.log(`error while adding user ${JSON.stringify(error)}`)
    }
}

export const getUsers = async () => {
    try {
        let response = await axios.get(`${url}/allusers`)
        return response.data;
    } catch (error) {
        console.log(`error while adding user ${JSON.stringify(error)}`)
    }
}



export const setConversation = async (data) => {
    try {
        let response = await axios.post(`${url}/conversation/add`, data)
        return response.data;
    } catch (error) {
        console.log(`error while setConversation api ${JSON.stringify(error)}`)
    }
}


export const newMessage = async (data) => {
    try {
        let response = await axios.post(`${url}/message/add`, data)
        return response.data;
    } catch (error) {
        console.log(`error while setConversation api ${JSON.stringify(error)}`)
    }
}

export const getAllMessages = async (id) => {
    try {
        let response = await axios.get(`${url}/message/get/${id}`)
        return response.data;
    } catch (error) {
        console.log(`error while setConversation api ${JSON.stringify(error)}`)
    }
}






