import React, { createContext, useEffect, useRef, useState } from 'react'
import { io } from 'socket.io-client'

export const AccontContext = createContext(null)




const AccontProvider = ({ children }) => {

    const [accont, setAccont] = useState()
    const [Person, SetPerson] = useState({})
    const socket = useRef(null)
    const [ActiveUsers, SetActiveUsers] = useState([])
    const [initialRender, setInitialRender] = useState(false)

    useEffect(() => {
        if (!initialRender) {
            socket.current = io('http://localhost:3333')
            setInitialRender(true)
        }
        return () => {
            socket.current?.off("connection")
        }
    }, [])


    return (
        <AccontContext.Provider
            value={{
                accont,
                setAccont,
                Person,
                SetPerson,
                socket,
                ActiveUsers,
                SetActiveUsers
            }}
        >
            {children}
        </AccontContext.Provider>
    )
}


export default AccontProvider